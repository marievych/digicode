<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

/**
 * Class CreateUser
 * A console command for creating new users
 *
 * @package App\Console\Commands
 */
class CreateUser extends Command
{
    /**
     * @var string
     */
    protected $signature = 'user:create {login} {password}';

    /**
     *
     */
    public function handle()
    {
        $login = $this->argument('login');
        $user = User::where(['login' => $login])->first();
        if (null !== $user){
            $this->error("User with login {$login} already exists.");
            exit(1);
        }
        $password = $this->argument('password');
        $hashedPassword = Hash::make(env('SALT', '') . $password);
        $user = new User();
        $user->login = $login;
        $user->password = $hashedPassword;
        $user->setRememberToken(null);
        if ($user->save())
        {
            $this->info('OK');
            exit(0);
        }
    }
}
