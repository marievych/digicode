<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

/**
 * Class MakeReport
 * A console command for making a report for the sum of all transactions per last day.
 *
 * @package App\Console\Commands
 */
class MakeReport extends Command
{
    /**
     * @var string
     */
    protected $signature = 'report';

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $date = Carbon::yesterday()->format('Y-m-d');
        $result = DB::select("select sum(amount) as sum from transactions where transactions.date > {$date}");
        $sum = $result[0]->sum;
        $file = fopen($date . '_report.txt', 'w');
        fwrite($file, "Date: {$date}. Sum: {$sum}");
        fclose($file);
    }
}
