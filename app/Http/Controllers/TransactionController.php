<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Transaction;
use App\Models\TransactionFilter;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class TransactionController
 *
 * @package App\Http\Controllers
 */
class TransactionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get transactions by filter
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request): JsonResponse
    {
        $this->validate($request, [
            'customerId' => 'integer|min:1',
            'dateFrom' => 'date',
            'dateTill' => 'date|after_or_equal:dateFrom',
            'amountMin' => 'integer|min:0',
            'amountMax' => 'integer|min:0',
            'offset' => 'integer|min:0',
            'limit' => 'integer|min:0',
        ]);

        $transactions = TransactionFilter::filter($request->all());

        return response()->json(['data' => $transactions->get()], 200);
    }

    /**
     * Get a transaction by customer id and transaction id
     *
     * @param $customerId
     * @param $transactionId
     *
     * @return JsonResponse
     */
    public function view($customerId, $transactionId): JsonResponse
    {
        $transaction = Transaction::where(['customer_id' => $customerId, 'transaction_id' => $transactionId])->firstOrFail();

        return response()->json($transaction, 200);
    }

    /**
     * Create new transaction
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $this->validate($request, [
            'customerId' => 'required|integer|min:0',
            'amount' => 'required|numeric',
        ]);
        $customerId = $request->get('customerId');
        $customer = Customer::where('customer_id', $customerId)->firstOrFail();
        if (null === $customer) {
            return response()->json(['errors' => ['customerId' => ['The customer is not found.']]], 404);
        }
        $transaction = new Transaction();
        $transaction->customerId = $customerId;
        $transaction->amount = round($request->get('amount'),2);
        $transaction->date = Carbon::now();
        if ($transaction->save()) {
            return response()->json(['data' => $transaction], 200);
        }
    }

    /**
     * Update a transaction
     *
     * @param int $transactionId
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(int $transactionId, Request $request): JsonResponse
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
        ]);
        $transaction = Transaction::where(['transaction_id' => $transactionId])->lockForUpdate()->firstOrFail();
        $transaction->amount = $request->get('amount');
        if (!$transaction->isDirty()) {
            return response()->json($transaction, 304);
        }
        $transaction->date = Carbon::now();
        $transaction->save();

        return response()->json($transaction, 200);
    }

    /**
     * Delete a transaction
     *
     * @param int $transactionId
     *
     * @return JsonResponse
     */
    public function delete(int $transactionId): JsonResponse
    {
        $transaction = Transaction::find($transactionId);
        $transaction->delete();

        return response()->json(null, 201);
    }
}
