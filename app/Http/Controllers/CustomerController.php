<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class CustomerController
 *
 * @package App\Http\Controllers
 */
class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Create new customer
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required|string',
            'cnp' => 'required|string|unique:customers,cnp',
        ]);
        $customer = new Customer($request->all());
        if ($customer->save()) {
            return response()->json(['data' => ['customerId' => $customer->customerId]]);
        }
    }
}
