<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

/**
 * Class SecurityController
 *
 * @package App\Http\Controllers
 */
class SecurityController extends Controller
{
    /**
     * Authorize the user.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws AuthorizationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'login' => 'string|required',
            'password' => 'string|required',
        ]);
        $login = $request->post('login');
        $password = $request->post('password');
        $user = User::where(['login' => $login])->first();
        if (null !== $user && Hash::check(env('SALT', '') . $password, $user->password)) {
            $user->rememberToken = str_random(100);
            Cache::put($user->rememberToken, $user->userId, env('API_TOKEN_TTL'));
            $user->tokenExpiresAt = Carbon::now()->addSeconds(env('API_TOKEN_TTL'));
            if ($user->save()) {
                return response()->json(['apiToken' => $user->rememberToken]);
            }
        } else {
            throw new AuthorizationException('Wrong login or password.', 401);
        }
    }
}
