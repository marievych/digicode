<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class Handler
 *
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     *
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $debug = env('APP_DEBUG', false);
        if (!$debug) {
            if ($exception instanceof ModelNotFoundException) {
                return response()->json(['error' => 'Not found.'], 404);
            } elseif ($exception instanceof ValidationException) {
                return response()->json(['error' => $exception->errors()], 400);
            } elseif ($exception instanceof AuthorizationException) {
                return response()->json(['error' => $exception->getMessage()], $exception->getCode());
            } elseif ($exception instanceof Exception) {
                return response()->json(['error' => 'Something went wrong.'], 500);
            }
        }

        return parent::render($request, $exception);
    }
}
