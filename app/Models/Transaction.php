<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * TRansaction model.
 *
 * @property mixed customerId
 * @property mixed amount
 * @property int date
 */
class Transaction extends BasicModel
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $primaryKey = 'transaction_id';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'customerId',
        'amount',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'date' => 'date:Y-m-d'
    ];

    /**
     * Get the transaction customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        return $this->hasOne(Customer::class, 'customer_id', 'customer_id');
    }
}
