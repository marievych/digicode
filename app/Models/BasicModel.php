<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class BasicModel
 * Basic model to extend by other models.
 *
 * @package App\Models
 */
class BasicModel extends Model
{
    /**
     * Set the attribute value.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return mixed|void
     */
    public function setAttribute($key, $value)
    {
        $this->attributes[Str::snake($key)] = $value;
    }

    /**
     * Get the attribute value.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getAttribute($key)
    {
        return $this->attributes[Str::snake($key)];
    }

    /**
     * Convert the model to array
     * @return array
     */
    public function toArray(): array
    {
        $result = [];
        foreach (parent::toArray() as $name => $value) {
            $result[Str::camel($name)] = $value;
        }

        return $result;
    }
}
