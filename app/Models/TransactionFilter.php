<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

class TransactionFilter
{
    public static function filter(array $filters): Builder
    {
        $transactions = Transaction::with('customer');
        if (isset($filters['customerId'])) {
            $transactions->where(['customerId' => $filters['customerId']]);
        }
        if (isset($filters['dateFrom'])) {
            $transactions->where('date', '>=', $filters['dateFrom']);
        }
        if (isset($filters['dateTill'])) {
            $transactions->where('date', '<=', $filters['dateTill']);
        }
        if (isset($filters['amountMin'])) {
            $transactions->where('amount', '>=', $filters['amountMin']);
        }
        if (isset($filters['amountMax'])) {
            $transactions->where('amount', '<=', $filters['amountMax']);
        }
        if (isset($filters['offset'])) {
            $transactions->offset($filters['offset']);
        }
        if (isset($filters['limit'])) {
            $transactions->limit($filters['limit']);
        }

        return $transactions;
    }
}
