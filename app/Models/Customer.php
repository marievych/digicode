<?php

namespace App\Models;

/**
 * Customer model
 *
 * @property int customerId
 * @property string $name
 * @property string cnp
 */
class Customer extends BasicModel
{
    /**
     * @var string
     */
    protected $primaryKey = 'customer_id';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'cnp',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get the customer transactions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'transaction_id', 'transaction_id');
    }
}
