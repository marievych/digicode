<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Transactions
 */
class Transactions extends Migration
{
    private const TABLE_NAME = 'transactions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(static::TABLE_NAME, function (Blueprint $table) {
            $table->increments('transaction_id');
            $table->unsignedInteger('customer_id');
            $table->double('amount', 10, 2);
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('customer_id')->references('customer_id')->on('customers')->onDelete('cascade');
            $table->index(['customer_id', 'amount', 'date'], 'index_customer_id_amount_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(static::TABLE_NAME);
    }
}
