<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/**
 * The Router instance.
 *
 * @var $router \Laravel\Lumen\Routing\Router
 */

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/customer', 'CustomerController@index');
    $router->post('/customer', 'CustomerController@create');

    $router->get('/transaction', 'TransactionController@index');
    $router->get('/transaction/{customerId:\d+}/{transactionId:\d+}', 'TransactionController@view');
    $router->post('/transaction', 'TransactionController@create');
    $router->patch('/transaction/{transactionId:\d+}', 'TransactionController@update');
    $router->delete('/transaction/{transactionId:\d+}', 'TransactionController@delete');

    $router->post('/security/login', 'SecurityController@login');
});
