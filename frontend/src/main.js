import Vue from "vue"
import VueRouter from "vue-router"
import VueCookie from "vue-cookie"
import App from "./App.vue"
import axios from "axios"
import "bootstrap"
import "bootstrap/scss/bootstrap.scss"
import "./assets/scss/main.scss"

Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.prototype.$http = axios;

Vue.use(VueRouter);
Vue.use(VueCookie);

import Transactions from "./components/Transactions"
import Login from "./components/Login"

const routes = [
    {path: "/login", component: Login},
    {path: "/", component: Transactions}
];

const router = new VueRouter({mode: "history", routes: routes});

new Vue({
    render: h => h(App),
    router:router
}).$mount("#app");
